#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 18:42:30 2021

@author: rohit
"""
import json

# import urllib library
from urllib.request import urlopen
import pprint
# store the URL in url as 
# parameter for urlopen
url = "https://us-central1-mmpdevlopment.cloudfunctions.net/getquestionviaID?data=AAB0F"
json_dict = {}
for i in range(1,99):
    # store the response of URL
    if(i >= 1 and i <= 9):
        suffix_url = '0' + str(i)
    else:
        suffix_url = str(i)
        
        response = urlopen(url + suffix_url)
      
        # storing the JSON response 
        # from url in data
        data_json = json.loads(response.read())
        json_dict[suffix_url] = data_json
        # print the json response

pprint.pprint(json_dict['10'])

with open('data.json', 'w') as fp:
    json.dump(json_dict, fp)
    
from pymongo import MongoClient

# Create connection to MongoDB
client = MongoClient('localhost', 27017)
db = client['name_of_database']
collection = db['name_of_collection']

# Build a basic dictionary
d = {'website': 'www.carrefax.com', 'author': 'Daniel Hoadley', 'colour': 'purple'}

# Insert the dictionary into Mongo
collection.insert(d)